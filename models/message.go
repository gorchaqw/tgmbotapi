package models

type Message struct {
	ChatId int    `json:"chat_id"`
	Text   string `json:"text"`
	ReplyToMessageId int `json:"reply_to_message_id"`
}
