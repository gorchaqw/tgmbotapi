package models

type Status struct {
	Ok bool `json:"ok"`
}

type ResultMessage struct {
	MessageID int `json:"message_id"`
	From      struct {
		ID           int    `json:"id"`
		IsBot        bool   `json:"is_bot"`
		FirstName    string `json:"first_name"`
		LastName     string `json:"last_name"`
		Username     string `json:"username"`
		LanguageCode string `json:"language_code"`
	} `json:"from"`
	Chat struct {
		ID        int    `json:"id"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Username  string `json:"username"`
		Type      string `json:"type"`
	} `json:"chat"`
	Date     int    `json:"date"`
	Text     string `json:"text"`
	Entities []struct {
		Offset int    `json:"offset"`
		Length int    `json:"length"`
		Type   string `json:"type"`
	} `json:"entities"`
}

type Result struct {
	UpdateID      int `json:"update_id"`
	ResultMessage `json:"message"`
}

type ResponseResult struct {
	Status
	Result Result `json:"result"`
}

type ResponseResults struct {
	Status
	Result []Result `json:"result"`
}
