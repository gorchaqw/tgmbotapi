package main

import (
	"errors"
	"fmt"
	"log"
	"os"
	"strconv"
	"strings"
)

type Env struct {
	Key     string
	Default string
	Value   string
}

func (a *App) getEnv(key, fallback string) *Env {
	e := Env{
		Key:     fmt.Sprintf("%s_%s", a.ProjectId, strings.TrimSpace(strings.ToUpper(key))),
		Default: fallback,
	}

	if err := e.parseEnv(); err != nil {
		log.Println(err)
	}

	return &e
}

func (e *Env) parseEnv() error {

	if value, ok := os.LookupEnv(e.Key); ok {
		e.Value = value
	} else {
		e.Value = e.Default
	}

	if e.Value == "" {
		return fmt.Errorf("zero Value : Key: '%s' Value: '%s' ", e.Key, e.Value)
	}

	return nil
}

func (e *Env) toInt() (int, error) {
	i, err := strconv.Atoi(e.Value)
	if err != nil {
		if _, ok := err.(*strconv.NumError); ok {
			return 0, fmt.Errorf("parsing Error : Key: '%s' Value: '%s' ", e.Key, e.Value)
		}

		return 0, errors.New("fail parse " + e.Key)
	}
	return i, nil
}

func (e *Env) toString() string {
	return e.Value
}

func (e *Env) toBool() bool {
	switch e.Value {
	case "true":
		return true
	default:
		return false
	}
}
