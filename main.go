package main

import (
	"log"
	"tgmbotapi/modules/api"
	apiUseCase "tgmbotapi/modules/api/usecase"
	"tgmbotapi/modules/message"
	messageUseCase "tgmbotapi/modules/message/usecase"
	"tgmbotapi/modules/processor"
	processorUseCase "tgmbotapi/modules/processor/usecase"
	"tgmbotapi/modules/update"
	updateUseCase "tgmbotapi/modules/update/usecase"
	"time"
)

type App struct {
	ProjectId string
	UseCase   struct {
		UpdateUseCase    update.UseCase
		ApiUseCase       api.UseCase
		MessageUseCase   message.UseCase
		ProcessorUseCase processor.UseCase
	}
	Config
}

type Config struct {
	Debug bool
	Token string
}

func initApp() *App {
	a := App{
		ProjectId: "TGM_BOT",
	}
	return &a
}

func main() {
	app := initApp()

	if err := app.initConfig(); err != nil {
		log.Println(err)
		return
	}

	app.initUseCases()

	if err := app.UseCase.UpdateUseCase.Start(); err != nil {
		log.Println(err)
	}

	if err := app.UseCase.ProcessorUseCase.Replay(); err != nil {
		log.Println(err)
	}

	time.Sleep(60 * time.Second)

	if err := app.UseCase.UpdateUseCase.Stop(); err != nil {
		log.Println(err)
	}
}

func (a *App) initConfig() error {
	cfg := Config{}
	cfg.Debug = a.getEnv("debug", "false").toBool()
	cfg.Token = a.getEnv("token", "1061693589:AAEpzPZI7yx9Q3ricFeQVH7J7JlhFCULVNk").toString()

	a.Config = cfg
	return nil
}

func (a *App) initUseCases() {
	a.UseCase.ApiUseCase = apiUseCase.NewApiUseCase(a.Token)

	a.UseCase.UpdateUseCase = updateUseCase.NewUpdateUseCase(a.UseCase.ApiUseCase)
	a.UseCase.MessageUseCase = messageUseCase.NewMessageUseCase(a.UseCase.ApiUseCase)

	a.UseCase.ProcessorUseCase = processorUseCase.NewProcessorUseCase(
		a.UseCase.ApiUseCase,
		a.UseCase.MessageUseCase,
		a.UseCase.UpdateUseCase)
}
