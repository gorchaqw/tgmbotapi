package processor

type UseCase interface {
	Replay() error
}
