package usecase

import (
	"log"
	"tgmbotapi/models"
	"tgmbotapi/modules/api"
	"tgmbotapi/modules/message"
	"tgmbotapi/modules/processor"
	"tgmbotapi/modules/update"
	"time"
)

type processorUseCase struct {
	API     api.UseCase
	Message message.UseCase
	Update  update.UseCase
}

func NewProcessorUseCase(api api.UseCase, message message.UseCase, update update.UseCase) processor.UseCase {
	return &processorUseCase{
		API:     api,
		Message: message,
		Update:  update,
	}
}

func (a *processorUseCase) Replay() error {

	ticker := time.NewTicker(time.Millisecond * 500)
	go func() {
		for range ticker.C {
			for k, u := range a.Update.GetUpdates() {

				if _, err := a.API.SendMessage(models.Message{
					ChatId:           u.Chat.ID,
					Text:             u.Text,
					ReplyToMessageId: u.MessageID,
				}); err != nil {
					log.Println(err)
				}

				a.Update.DelUpdate(k)
			}
		}
	}()

	return nil
}
