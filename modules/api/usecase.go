package api

import "tgmbotapi/models"

type UseCase interface {
	GetMe() error
	GetUpdates(body models.Update) (*models.ResponseResults, error)
	SendMessage(body models.Message) (*models.ResponseResult, error)
}
