package usecase

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"tgmbotapi/models"
	"tgmbotapi/modules/api"
)

type apiUseCase struct {
	Token       string
}

func NewApiUseCase(token string) api.UseCase {
	return &apiUseCase{
		Token:       token,
	}
}

func (u *apiUseCase) GetMe() error {
	resp, err := u.client(u.Token, "getMe", nil)
	if err != nil {
		return err
	}

	log.Println(resp.StatusCode)

	if resp.StatusCode == http.StatusOK {
		bodyBytes, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}
		bodyString := string(bodyBytes)
		log.Println(bodyString)
	}
	return nil
}

func (u *apiUseCase) SendMessage(body models.Message) (*models.ResponseResult, error) {
	var uResp models.ResponseResult

	b, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	resp, err := u.client(u.Token, "sendMessage", bytes.NewReader(b))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {

		if err := json.NewDecoder(resp.Body).Decode(&uResp); err != nil {
			return nil, err
		}

		return &uResp, nil
	}
	return nil, http.ErrBodyNotAllowed
}

func (u *apiUseCase) GetUpdates(body models.Update) (*models.ResponseResults, error) {
	var uResp models.ResponseResults

	b, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	resp, err := u.client(u.Token, "getUpdates", bytes.NewReader(b))
	if err != nil {
		return nil, err
	}

	if resp.StatusCode == http.StatusOK {

		if err := json.NewDecoder(resp.Body).Decode(&uResp); err != nil {
			return nil, err
		}

		return &uResp, nil
	}
	return nil, http.ErrBodyNotAllowed
}

func (u *apiUseCase) client(token, method string, body io.Reader) (*http.Response, error) {
	client := &http.Client{}

	url := fmt.Sprintf("https://api.telegram.org/bot%s/%s", token, method)

	req, err := http.NewRequest("POST", url, body)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	return resp, nil
}
