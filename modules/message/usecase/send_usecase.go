package usecase

import (
	"tgmbotapi/models"
	"tgmbotapi/modules/api"
	"tgmbotapi/modules/message"
	"tgmbotapi/modules/update"
)

type messageUseCase struct {
	API    api.UseCase
	Update update.UseCase
}

func NewMessageUseCase(api api.UseCase) message.UseCase {
	return &messageUseCase{
		API:    api,
	}
}

func (a *messageUseCase) Send(msg models.Message) error {
	if _, err := a.API.SendMessage(msg); err != nil {
		return err
	}
	return nil
}
