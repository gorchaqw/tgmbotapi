package message

import "tgmbotapi/models"

type UseCase interface {
	Send(msg models.Message) error
}
