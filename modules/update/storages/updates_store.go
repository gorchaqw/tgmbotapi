package storages

import (
	"sync"
	"tgmbotapi/models"
)

type UpdateStorage struct {
	sync.Mutex
	Store map[int]models.ResultMessage
}

func NewUpdateStorage() *UpdateStorage {
	return &UpdateStorage{Store: make(map[int]models.ResultMessage)}
}

func (s *UpdateStorage) Add(updateID int, msg models.ResultMessage) {
	s.Lock()
	s.Store[updateID] = msg
	defer s.Unlock()
}

func (s *UpdateStorage) Del(updateID int) {
	s.Lock()
	delete(s.Store, updateID)
	defer s.Unlock()
}

func (s *UpdateStorage) Get(updateID int) models.ResultMessage {
	s.Lock()
	defer s.Unlock()
	return s.Store[updateID]
}

func (s *UpdateStorage) GetStore() map[int]models.ResultMessage {
	s.Lock()
	defer s.Unlock()
	return s.Store
}
