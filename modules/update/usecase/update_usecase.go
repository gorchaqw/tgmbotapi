package usecase

import (
	"log"
	"tgmbotapi/models"
	"tgmbotapi/modules/api"
	"tgmbotapi/modules/update"
	"tgmbotapi/modules/update/storages"
	"time"
)

type updateUseCase struct {
	Ticker *time.Ticker
	API    api.UseCase
	Store  *storages.UpdateStorage
}

func NewUpdateUseCase(api api.UseCase) update.UseCase {
	return &updateUseCase{
		Ticker: time.NewTicker(time.Millisecond * 500),
		API:    api,
		Store:  storages.NewUpdateStorage(),
	}
}

func (a *updateUseCase) Start() error {
	go func() {
		var offset int
		for range a.Ticker.C {

			updates, err := a.API.GetUpdates(models.Update{
				Offset: offset,
			})

			if err != nil {
				log.Println(err)
				continue
			}

			for _, result := range updates.Result {
				if result.UpdateID-offset == 1 || offset == 0 {
					offset = result.UpdateID
					a.Store.Add(result.UpdateID, result.ResultMessage)
				}
			}
		}
	}()
	return nil
}

func (a *updateUseCase) Stop() error {
	a.Ticker.Stop()
	log.Println("Ticker stopped")
	return nil
}

func (a *updateUseCase) GetUpdates() map[int]models.ResultMessage {
	return a.Store.GetStore()
}

func (a *updateUseCase) DelUpdate(updateId int) {
	a.Store.Del(updateId)
}
