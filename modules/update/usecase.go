package update

import (
	"tgmbotapi/models"
)

type UseCase interface {
	Start() error
	Stop() error
	GetUpdates() map[int]models.ResultMessage
	DelUpdate(updateId int)
}
